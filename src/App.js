import React, { useState } from 'react';
import './App.css';

const App = () =>  {
  const [collection] = useState([4, 8, 0, 9, 2, 5, 0, 3, 3, 0]);
  const [zerosToTheRight, setZerosToTheRight] = useState([]);
  const [withoutZeros, setWithoutZeros] = useState([]);
  const [withoutZerosPlusOne, setWithoutZerosPlusOne] = useState([]);
  const [withoutZerosPlusMinusOne, setWithoutZerosPlusMinusOne] = useState([]);

  const displayArray = (arr) => {
    return `[${arr.map(item => item)}]`;
  }

  const moveZerosToTheRight = () => {
    const noZeros = collection.filter(items => items !== 0);
    const withZeros = noZeros.concat(Array(collection.length - noZeros.length).fill(0));

    setWithoutZeros(noZeros);
    setZerosToTheRight(withZeros);

    console.log(noZeros, 'Without Zeros');
    console.log(withZeros, 'With Zeros');
  };

  const plusOneToWithoutZeros = () => {
    if(withoutZeros.length) {
      const plusOne = (parseInt(withoutZeros.join(''))+1).toString().split('').map(Number);

      console.log(plusOne, 'Without Zeros Plus One');

      setWithoutZerosPlusOne(plusOne);
    }
  };

  const multiplyByMinusOne = () => {
    const multMinusOne = withoutZeros.map((item, index) => index % 2 !== 0 ? item * -1 : item);

    console.log(multMinusOne, 'Multiply By Minus One');

    setWithoutZerosPlusMinusOne(multMinusOne);
  };

  const getMaxSubArrSum = () => {
    if(withoutZerosPlusMinusOne.length) {
      let max = Number.MIN_SAFE_INTEGER;
      let currentMax = 0

      withoutZerosPlusMinusOne.forEach(item => {
        currentMax += item;

        if (max < currentMax) {
          max = currentMax;
        }

        if (currentMax < 0) {
          currentMax = 0;
        }
      });

      console.log(max, 'Get Max Sub Arr Sum');

      return max;
    }

    return 'N/A';
  };

  return (
    <div className='app'>
      <header className="app-header">
        <h1>JS Challenge</h1>

        <p>
          <b>Initial collection:</b> {displayArray(collection)}
        </p>

        <button onClick={moveZerosToTheRight}>
          Move Zeros To The Right
        </button>

        <hr/>

        <p>
          <b>Zeros To The Right:</b> {displayArray(zerosToTheRight)}
        </p>

        <hr/>

        <p>
          <b>Without Zeros:</b> {displayArray(withoutZeros)}
        </p>

        <hr/>
        <button onClick={plusOneToWithoutZeros} disabled={!withoutZeros.length}>
          Get Without Zeros Plus One
        </button>

        <p>
          <b>Without Zeros Plus One:</b> {displayArray(withoutZerosPlusOne)}
        </p>

        <hr/>
        <p className='large-margin-bottom'>Multiply Without Zeros Odd position by -1 & Maximum Sum Of Digits</p>
        <button onClick={multiplyByMinusOne} disabled={!withoutZeros.length} className='large-margin-bottom'>
          Calculate
        </button>

        <p>
          <b>Multiply Without Zeros Odd position by -1:</b> {displayArray(withoutZerosPlusMinusOne)}
        </p>

        <p>
          <b>Maximum Sum Of Sub Array Digits:</b> {getMaxSubArrSum()}
        </p>
      </header>
    </div>
  );
}

export default App;
